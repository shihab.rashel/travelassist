﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using TravelAssist.AppModel;
using TravelAssist.AppModel.AppModel;
using TravelAssist.Service.Interfaces;

namespace TravelAssist.Api.Controllers
{
    [Route("api/v{version:apiVersion}/comment")]
    [ApiController]
    public class CommentController : ControllerBase
    {
        private readonly ICommentService _commentService;
        public CommentController(ICommentService commentService)
        {
            _commentService = commentService;
        }
        [HttpGet("{touristPlaceId}")]
        public virtual ActionResult<List<AppComment>> GetCommentByTouristPlaceId(int touristPlaceId)
        {
            var data = _commentService.GetCommentByTouristPlaceId(touristPlaceId);
            if (data != null)
                return Ok(data);
            return NotFound();
        }

        [HttpPost]
        public virtual ActionResult<CrudResult> AddComment(AppComment comment)
        {
            return _commentService.Add(comment);
        }
    }
}
