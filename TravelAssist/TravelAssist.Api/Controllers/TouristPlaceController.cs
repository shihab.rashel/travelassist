﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Mvc;
using TravelAssist.AppModel;
using TravelAssist.AppModel.AppModel;
using TravelAssist.Service.Interfaces;

namespace TravelAssist.Api.Controllers
{
    [Route("api/v{version:apiVersion}/tourist-place")]
    [ApiController]
    public class TouristPlaceController : ControllerBase
    {
        private readonly ITouristPlaceService _touristPlaceService;
        public TouristPlaceController(ITouristPlaceService touristPlaceService)
        {
            _touristPlaceService = touristPlaceService;
        }
        [HttpGet]
        public virtual ActionResult<List<AppTouristPlace>> GetAll()
        {
            var data = _touristPlaceService.GetAll();
            if (data != null)
                return Ok(data);
            return NotFound();
        }
        [HttpPost]
        public virtual ActionResult<CrudResult> Add(AppTouristPlace touristPlace)
        {
            return _touristPlaceService.Add(touristPlace);
        }
        [HttpPut]
        public virtual ActionResult<CrudResult> Edit(AppTouristPlace touristPlace)
        {
            return _touristPlaceService.Edit(touristPlace);
        }
        [HttpDelete("{id}")]
        public virtual ActionResult<CrudResult> Delete(int id)
        {
            return _touristPlaceService.Delete(id);
        }
        [HttpPost("photo")]
        public virtual ActionResult<CrudResult> UploadPhoto()
        {
            try
            {
                if (Request.Form.Files.Any())
                {
                    var file = Request.Form.Files[0];
                    var folderName = Path.Combine("Resources", "TouristPlace");
                    var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                    if (file.Length > 0)
                    {
                        var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                        var fullPath = Path.Combine(pathToSave, fileName);
                        if (System.IO.File.Exists(fullPath))
                            System.IO.File.Delete(fullPath);
                        using (var stream = new FileStream(fullPath, FileMode.Create))
                        {
                            file.CopyTo(stream);
                        }
                    }
                }
                return Ok(new CrudResult(true, "Photo uploaded successfully"));
            }
            catch (Exception ex)
            {
                return Ok(new CrudResult(false, "Something went wrong while upload photo"));
            }

        }
    }
}
