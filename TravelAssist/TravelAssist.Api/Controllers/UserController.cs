﻿using Microsoft.AspNetCore.Mvc;
using TravelAssist.AppModel;
using TravelAssist.AppModel.AppModel;
using TravelAssist.Service.Interfaces;

namespace TravelAssist.Api.Controllers
{
    [Route("api/v{version:apiVersion}/user")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost]
        public virtual ActionResult<CrudResult> Register(AppUser user)
        {
            return _userService.RegisterUser(user);
        }

        [HttpGet("phone-number-exist/{phoneNumber}")]
        public virtual ActionResult<bool> IsPhoneNumberExist(string phoneNumber)
        {
            return _userService.IsPhoneNumberExist(phoneNumber);
        }
        [HttpGet("email-exist/{email}")]
        public virtual ActionResult<bool> IsEmailExist(string email)
        {
            return _userService.IsEmailExist(email);
        }
    }
}
