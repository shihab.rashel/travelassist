import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../service/user.service';
import { AuthenticationService } from "../service/authentication.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginObject: any = {
    EmailOrPhone: null,
    Password: null,
    User:null
  }
  constructor(private userService: UserService, private authenticationService: AuthenticationService, private router: Router) { 
    this.loginObject.Email='';
    this.loginObject.Password='';
    this.loginObject.User=-1;
  }
  login() {
    this.authenticationService.login(this.loginObject.EmailOrPhone, this.loginObject.Password).subscribe((res: any) => {
        if (res) {
          console.log(res);
          alert("Logged in successful");
          localStorage.setItem('currentUser', JSON.stringify(res));
          var localUser = JSON.parse(localStorage.getItem('currentUser'));
          console.log(localUser);
          this.router.navigate(['/tourist-place']);
        } else {
          alert("Logged in unsuccessful");
          console.log(res);
        }
      }
    );
  }
  ngOnInit(): void {
  }
  register() {
    this.router.navigate(['/registration']);
  }
}
