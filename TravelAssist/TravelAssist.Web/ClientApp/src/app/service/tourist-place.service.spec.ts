import { TestBed } from '@angular/core/testing';

import { TouristPlaceService } from './tourist-place.service';

describe('TouristPlaceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TouristPlaceService = TestBed.get(TouristPlaceService);
    expect(service).toBeTruthy();
  });
});
