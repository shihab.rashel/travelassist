import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TouristPlace } from "../model/tourist-place.model";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class TouristPlaceService {
  add(data:TouristPlace) {
    return this.http.post(environment.apiEndPoint + "tourist-place", data);
  }
  uploadPhoto(formData:FormData) {
    return this.http.post(environment.apiEndPoint + "tourist-place/photo", formData);
  }
  constructor(private http: HttpClient) { }

  edit(touristPlace: TouristPlace) {
    return this.http.put(environment.apiEndPoint + "tourist-place", touristPlace);
  }
  delete(id: number) {
    return this.http.delete(environment.apiEndPoint + "tourist-place/"+id);
  }

  getAll() { return this.http.get(environment.apiEndPoint + "tourist-place") }
}
