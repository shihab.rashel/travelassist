import { Injectable } from '@angular/core';
import { environment } from "../../environments/environment";
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  constructor(private http: HttpClient) {

  }
  login(email, password) {
    let options = { headers: environment.header };
    //return this.http.post<any>(environment.apiEndPointRoot + 'authentication/login?userIdOrEmail=' + email + '&password=' + password, options).pipe(
    //  map(user => {
    //    user.authdata = user.Token;
    //    localStorage.setItem('currentUser', JSON.stringify(user));
    //    return user;
    //  })
    //);
    return this.http.get(environment.apiEndPoint + "authentication/login?emailOrPassword=" + email + "&password=" + password);
  }
}
