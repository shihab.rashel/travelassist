import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Comment } from '../model/comment.model';
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class CommentService {
  getCommentsByPlaceId(id: number) {
    return this.http.get(environment.apiEndPoint + "comment/" + id);
  }

  constructor(private http:HttpClient) { }

  add(comment:Comment) {
    return this.http.post(environment.apiEndPoint + "comment", comment);
  }
}
