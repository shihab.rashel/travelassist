import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Registration } from '../model/registration.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }
  register(data: Registration){
    return this.http.post(environment.apiEndPoint + "user", data);
  }
  isPhoneNumberExist(phoneNumber: string) {
    return this.http.get(environment.apiEndPoint + "user/phone-number-exist/" + phoneNumber);
  }
  isEmailExist(email: string) {
    return this.http.get(environment.apiEndPoint + "user/email-exist/" + email);
  }
}
