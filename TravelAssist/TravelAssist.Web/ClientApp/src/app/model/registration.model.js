"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Registration = void 0;
var Registration = /** @class */ (function () {
    function Registration() {
        this.Id = 0;
        this.Email = '';
        this.PhoneNumber = '';
        this.Password = '';
    }
    return Registration;
}());
exports.Registration = Registration;
//# sourceMappingURL=registration.model.js.map