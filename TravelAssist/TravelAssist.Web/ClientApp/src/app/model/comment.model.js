"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Comment = void 0;
var Comment = /** @class */ (function () {
    function Comment() {
        this.Id = 0;
        this.Text = '';
        this.UserId = 0;
        this.TouristPlaceId = 0;
    }
    return Comment;
}());
exports.Comment = Comment;
//# sourceMappingURL=comment.model.js.map