export class TouristPlace{
    Id:number;
    Title:string;
    Description:string;
    Image:string;
  UserId: number;
  IsAbleToAction:boolean;
    constructor(){
        this.Id=0;
        this.Title='';
        this.Description='';
        this.Image='';
      this.UserId = -1;
      this.IsAbleToAction = false;
    }
}
