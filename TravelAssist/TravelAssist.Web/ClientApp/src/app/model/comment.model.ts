export class Comment {
  Id: number;
  Text: string;
  UserId: number;
  TouristPlaceId: number;
  constructor() {
    this.Id = 0;
    this.Text = '';
    this.UserId = 0;
    this.TouristPlaceId = 0;
  }
}
