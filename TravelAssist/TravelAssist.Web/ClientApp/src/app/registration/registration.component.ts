import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Registration } from "../model/registration.model";
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  registration: Registration;
  phoneNumberExist: boolean = false;
  emailExist: boolean = false;
  constructor(private userService:UserService,private router:Router) {
    this.registration = new Registration();
  }
register() {
  this.userService.register(this.registration).subscribe((res: any) => {
    console.log(res);
    if (res) {
      if (res.Success) {
        alert("Registration successful");
        this.router.navigate([""]);
      }
    }
  });
}
isPhoneNumberExist() {
  this.userService.isPhoneNumberExist(this.registration.PhoneNumber).subscribe((res: any) => {
    this.phoneNumberExist = res;
  });
}
isEmailExist() {
  this.userService.isEmailExist(this.registration.Email).subscribe((res: any) => {
    this.emailExist = res;
  });
}
  ngOnInit(): void {
  }

}
