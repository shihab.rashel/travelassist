import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { TouristPlace } from '../model/tourist-place.model';
import { Comment } from '../model/comment.model';
import { TouristPlaceService } from "../service/tourist-place.service";
import { CommentService } from "../service/comment.service";

@Component({
  selector: 'app-tourist-place',
  templateUrl: './tourist-place.component.html',
  styleUrls: ['./tourist-place.component.css']
})
export class TouristPlaceComponent implements OnInit {
touristPlaceList:Array<TouristPlace>;
touristPlace:TouristPlace;
  touristPlaceToAdd: TouristPlace;
  comments: Array<Comment>;
  comment:Comment;
fileData: File ;
previewUrl: any ;
  isAdd: boolean;
  user: any;
  currentTouristPlaceIndex:number;
  constructor(private touristPlaceService: TouristPlaceService,private commentService:CommentService) {
    this.touristPlace=new TouristPlace();
    this.touristPlaceToAdd=new TouristPlace();
    this.touristPlaceList = [];
    this.comment = new Comment();
    this.comments = [];
    this.fileData=null;
    this.previewUrl=null;
    this.isAdd = false;

    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.currentTouristPlaceIndex = 0;
  }

  ngOnInit(): void {
    this.getAll();
  }
  getAll() {
    this.touristPlaceService.getAll().subscribe((res: any) => {
      this.touristPlaceList = res;
      this.touristPlaceList.forEach(t => {
        t.IsAbleToAction = this.user.Id == t.UserId;
      });
      this.touristPlace = this.touristPlaceList.length > 0 ? this.touristPlaceList[0] : new TouristPlace();
      this.getCommentsByPlaceId();
    });
  }
  getCommentsByPlaceId() {
    this.comments = [];
    this.commentService.getCommentsByPlaceId(this.touristPlace.Id).subscribe((res: any) => {
      this.comments = res;
    });
  }

  constructImage(photo) {
    if (!photo)
      photo = 'default-image.png';
    return environment.apiEndPointRoot + 'Resources/TouristPlace/' + photo;
  }
  fileProgress(fileInput: any) {
    this.fileData = <File>fileInput.target.files[0];
    this.preview();
  }
  preview() {
    var mimeType = this.fileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }
    var reader = new FileReader();
    reader.readAsDataURL(this.fileData);
    reader.onload = (_event) => {
      this.previewUrl = reader.result;
    }
  }
  addNewClicked(){
    this.isAdd=true;
    this.touristPlaceToAdd=new TouristPlace();
  }
  goToListClicked(){
    this.isAdd=false;
    this.touristPlace=this.touristPlaceList.length>0?this.touristPlaceList[0]:new TouristPlace();
  }
  previousClicked() {
    if (this.currentTouristPlaceIndex !== 0) {
      this.currentTouristPlaceIndex--;
      this.touristPlace = this.touristPlaceList[this.currentTouristPlaceIndex];
      this.getCommentsByPlaceId();
    }

  }
  nextClicked() {
    if (this.currentTouristPlaceIndex !== this.touristPlaceList.length - 1) {
      this.currentTouristPlaceIndex++;
      this.touristPlace = this.touristPlaceList[this.currentTouristPlaceIndex];
      this.getCommentsByPlaceId();
    }
  }
  saveTouristPlace() {
    let fileToUpload = this.fileData;
    const fData = new FormData();
    if (fileToUpload != null) {
      this.touristPlaceToAdd.Image = this.user.Id  + '_' + fileToUpload.name;
      fData.append('file', fileToUpload, this.user.Id + '_' + fileToUpload.name);
    }
    this.touristPlaceToAdd.UserId = this.user.Id;
    this.touristPlaceService.uploadPhoto(fData).subscribe((pRes: any) => {
      if (pRes && pRes.Success) {
        this.touristPlaceService.add(this.touristPlaceToAdd).subscribe((res: any) => {
          if (res) {
            if (res.Success) {
              alert("Tourist place added successfully");
              this.isAdd = false;
              this.getAll();
            } else {
              alert("Error while adding tourist place. Please try again");
            }
          } else {
            alert("Error while adding tourist place. Please try again");
          }
        });
      } else {
        alert("Something went wrong while uploading photo");
      }
    });
    
  }
  editTouristPlace() {
    this.touristPlace.UserId = this.user.Id;
    this.touristPlaceService.edit(this.touristPlace).subscribe((res: any) => {
      if (res) {
        if (res.Success) {
          alert("Tourist place edited successfully");
          this.isAdd = false;
          this.getAll();
        }
      } else {
        alert("Error while editing tourist place. Please try again");
      }
    });
  }
  deleteTouristPlace() {
    this.touristPlaceService.delete(this.touristPlace.Id).subscribe((res: any) => {
      if (res) {
        if (res.Success) {
          alert("Tourist place deleted successfully");
          this.isAdd = false;
          this.getAll();
        }
      } else {
        alert("Error while deleting tourist place. Please try again");
      }
    });
  }
  addComment() {
    this.comment.UserId = this.user.Id;
    this.comment.TouristPlaceId = this.touristPlace.Id;
    this.commentService.add(this.comment).subscribe((res: any) => {
      if (res && res.Success) {
        alert("Commented successfully");
        this.getCommentsByPlaceId();
      }
      else
        alert("Something went wrong while commenting. Please try again");
    });
  }
}
