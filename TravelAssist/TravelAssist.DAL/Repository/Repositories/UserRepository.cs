﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using TravelAssist.DAL.Models;
using TravelAssist.DAL.Repository.Interfaces;

namespace TravelAssist.DAL.Repository.Repositories
{
    public partial class UserRepository: IUserRepository
    {
        private readonly DbContextOptionsBuilder<TravelAssistDbContext> _dbContextOptionBuilder;
        public UserRepository(string connectionString)
        {
            _dbContextOptionBuilder = new DbContextOptionsBuilder<TravelAssistDbContext>();
            _dbContextOptionBuilder.UseSqlServer(connectionString);
        }

        public virtual User GetUserByEmailOrPhoneAndPassword(string emailOrPhone, string password)
        {
            using var context = new TravelAssistDbContext(_dbContextOptionBuilder.Options);
            return context.Users.FirstOrDefault(u=>(u.Email==emailOrPhone ||u.PhoneNumber==emailOrPhone) && u.Password==password);
        }

        public virtual bool IsPhoneNumberExist(string phoneNumber)
        {
            using var context = new TravelAssistDbContext(_dbContextOptionBuilder.Options);
            return context.Users.Any(u => u.PhoneNumber == phoneNumber);
        }

        public virtual bool IsEmailExist(string email)
        {
            using var context = new TravelAssistDbContext(_dbContextOptionBuilder.Options);
            return context.Users.Any(u => u.Email == email);
        }
    }
}
