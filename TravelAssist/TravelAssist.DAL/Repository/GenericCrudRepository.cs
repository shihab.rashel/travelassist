﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using TravelAssist.DAL.Models;

namespace TravelAssist.DAL.Repository
{
    public partial class GenericCrudRepository: IGenericCrudRepository
    {
        private readonly TravelAssistDbContext _dbContext;

        public GenericCrudRepository(string connectionString)
        {
            var dbContextOptionBuilder = new DbContextOptionsBuilder<TravelAssistDbContext>();
            dbContextOptionBuilder.UseSqlServer(connectionString);
            _dbContext = new TravelAssistDbContext(dbContextOptionBuilder.Options);
        }
        public virtual T Get<T>(int id) where T : class
        {
            var dbSet = _dbContext.Set<T>();
            var entity = dbSet.Find(id);
            return entity;
        }

        public virtual List<T> GetAll<T>() where T : class
        {
            var dbSet = _dbContext.Set<T>();
            var entityList = dbSet.ToList();
            return entityList;
        }

        public virtual T Add<T>(T entity) where T : class
        {
            var dbSet = _dbContext.Set<T>();
            dbSet.Add(entity);
            _dbContext.SaveChanges();
            return entity;
        }

        public virtual T Edit<T>(T entity) where T : class
        {
            _dbContext.Entry(entity).State = EntityState.Modified;
            _dbContext.SaveChanges();
            return entity;
        }

        public virtual T Delete<T>(int id) where T : class
        {
            var dbSet = _dbContext.Set<T>();
            var entity = dbSet.Find(id);
            dbSet.Remove(entity);
            _dbContext.SaveChanges();
            return entity;
        }

        public virtual List<T> GetByProperty<T>(Expression<Func<T, bool>> predicate) where T : class
        {
            var dbSet = _dbContext.Set<T>();
            var entityList = dbSet.Where(predicate).ToList();
            return entityList;
        }
    }
}
