﻿using System;
using System.Collections.Generic;

namespace TravelAssist.DAL.Repository
{
    public partial interface IGenericCrudRepository
    {
        T Get<T>(int id) where T : class;
        List<T> GetAll<T>() where T : class;
        T Add<T>(T entity) where T : class;
        T Edit<T>(T entity) where T : class;
        T Delete<T>(int id) where T : class;
        List<T> GetByProperty<T>(System.Linq.Expressions.Expression<Func<T, bool>> predicate) where T : class;
    }
}
