﻿using TravelAssist.DAL.Models;

namespace TravelAssist.DAL.Repository.Interfaces
{
    public partial interface IUserRepository
    {
        User GetUserByEmailOrPhoneAndPassword(string emailOrPhone, string password);
        bool IsPhoneNumberExist(string phoneNumber);
        bool IsEmailExist(string email);
    }
}
