﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TravelAssist.DAL.Models
{
    public partial class User
    {
        public User()
        {
            Comments = new HashSet<Comment>();
            TouristPlaces = new HashSet<TouristPlace>();
        }

        public int Id { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Password { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<TouristPlace> TouristPlaces { get; set; }
    }
}
