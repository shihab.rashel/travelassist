﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TravelAssist.DAL.Models
{
    public partial class TouristPlace
    {
        public TouristPlace()
        {
            Comments = new HashSet<Comment>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public int UserId { get; set; }

        public virtual User User { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
    }
}
