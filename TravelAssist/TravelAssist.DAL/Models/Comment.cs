﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TravelAssist.DAL.Models
{
    public partial class Comment
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public int UserId { get; set; }
        public int TouristPlaceId { get; set; }

        public virtual TouristPlace TouristPlace { get; set; }
        public virtual User User { get; set; }
    }
}
