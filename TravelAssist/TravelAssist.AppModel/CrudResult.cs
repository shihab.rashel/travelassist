﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TravelAssist.AppModel
{
    public partial class CrudResult
    {
        public CrudResult()
        {

        }
        public CrudResult(bool success, string message,string errorMessage= "")
        {
            Success = success;
            Message = message;
            ErrorMessage = errorMessage;
        }
        public string Message { get; set; }
        public string ErrorMessage { get; set; }
        public bool Success { get; set; }
        public long Id { get; set; }
        public dynamic Entity { get; set; }
    }
}
