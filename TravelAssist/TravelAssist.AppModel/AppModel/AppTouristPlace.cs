﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TravelAssist.AppModel.AppModel
{
    public partial class AppTouristPlace
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public int UserId { get; set; }
    }
}
