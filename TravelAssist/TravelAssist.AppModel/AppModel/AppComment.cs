﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TravelAssist.AppModel.AppModel
{
    public partial class AppComment
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public int UserId { get; set; }
        public int TouristPlaceId { get; set; }
    }
}
