﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TravelAssist.AppModel.AppModel
{
    public partial class AppUser
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Password { get; set; }
    }
}
