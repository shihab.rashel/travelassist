﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TravelAssist.AppModel;

namespace TravelAssist.Service
{
    public partial interface IGenericCrudService
    {
        T Get<T>(int id) where T : class;
        List<T> GetAll<T>() where T : class;
        List<T> GetByProperty<T>(Expression<Func<T, bool>> predicate) where T : class;
        CrudResult Add<T>(T entity) where T : class;
        CrudResult Edit<T>(T entity) where T : class;
        CrudResult Delete<T>(int id) where T : class;
    }
}
