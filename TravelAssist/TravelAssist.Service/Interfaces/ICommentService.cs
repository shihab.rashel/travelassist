﻿using System.Collections.Generic;
using TravelAssist.AppModel;
using TravelAssist.AppModel.AppModel;

namespace TravelAssist.Service.Interfaces
{
    public partial interface ICommentService
    {
        CrudResult Add(AppComment appComment);
        List<AppComment> GetCommentByTouristPlaceId(int touristPlaceId);
    }
}
