﻿using System.Collections.Generic;
using TravelAssist.AppModel;
using TravelAssist.AppModel.AppModel;

namespace TravelAssist.Service.Interfaces
{
    public partial interface ITouristPlaceService
    {
        List<AppTouristPlace> GetAll();
        CrudResult Add(AppTouristPlace appTouristPlace);
        CrudResult Edit(AppTouristPlace appTouristPlace);
        CrudResult Delete(int touristPlaceId);
    }
}
