﻿using TravelAssist.AppModel;
using TravelAssist.AppModel.AppModel;

namespace TravelAssist.Service.Interfaces
{
    public partial interface IUserService
    {
        CrudResult RegisterUser(AppUser appUser);
        AppUser GetUserById(int userId);
        AppUser GetUserByEmailOrPhoneAndPassword(string emailOrPhone, string password);
        bool IsPhoneNumberExist(string phoneNumber);
        bool IsEmailExist(string email);
    }
}
