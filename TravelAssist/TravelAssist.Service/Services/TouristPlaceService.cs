﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using TravelAssist.AppModel;
using TravelAssist.AppModel.AppModel;
using TravelAssist.DAL.Models;
using TravelAssist.Service.Interfaces;

namespace TravelAssist.Service.Services
{
    public partial class TouristPlaceService : GenericCrudService, ITouristPlaceService
    {
        private readonly MapperConfiguration _config;
        public TouristPlaceService(string connectionString) : base(connectionString)
        {
            _config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<TouristPlace, AppTouristPlace>();
                cfg.CreateMap<AppTouristPlace, TouristPlace>();
            });
        }

        public virtual CrudResult Add(AppTouristPlace appTouristPlace)
        {
            var mapper = _config.CreateMapper();
            var touristPlace = mapper.Map<AppTouristPlace, TouristPlace>(appTouristPlace);
            return Add(touristPlace);
        }

        public virtual CrudResult Delete(int touristPlaceId)
        {
            return Delete<TouristPlace>(touristPlaceId);
        }

        public virtual CrudResult Edit(AppTouristPlace appTouristPlace)
        {
            var mapper = _config.CreateMapper();
            var touristPlace = mapper.Map<AppTouristPlace, TouristPlace>(appTouristPlace);
            return Edit(touristPlace);
        }

        public virtual List<AppTouristPlace> GetAll()
        {
            var touristPlaceList=GetAll<TouristPlace>();
            var mapper = _config.CreateMapper();
            var appTouristPlaceList = touristPlaceList.Select(e => mapper.Map<TouristPlace, AppTouristPlace>(e)).ToList();
            return appTouristPlaceList;
        }
    }
}
