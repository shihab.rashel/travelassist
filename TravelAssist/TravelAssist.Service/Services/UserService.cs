﻿using AutoMapper;
using TravelAssist.AppModel;
using TravelAssist.AppModel.AppModel;
using TravelAssist.DAL.Models;
using TravelAssist.DAL.Repository.Interfaces;
using TravelAssist.Service.Interfaces;

namespace TravelAssist.Service.Services
{
    public partial class UserService : GenericCrudService,IUserService
    {
        private readonly MapperConfiguration _config;
        private readonly IUserRepository _userRepository;
        public UserService(string connectionString,IUserRepository userRepository) : base(connectionString)
        {
            _userRepository = userRepository;
            _config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<User, AppUser>();
                cfg.CreateMap<AppUser, User>();
            });
        }

        public virtual CrudResult RegisterUser(AppUser appUser)
        {
            var mapper = _config.CreateMapper();
            var user = mapper.Map<AppUser, User>(appUser);
            return Add(user);
        }

        public virtual AppUser GetUserById(int userId)
        {
            var dbUser = Get<User>(userId);
            var mapper = _config.CreateMapper();
            var appUser = mapper.Map<User, AppUser>(dbUser);
            return appUser;
        }

        public virtual AppUser GetUserByEmailOrPhoneAndPassword(string emailOrPhone, string password)
        {
            var dbUser = _userRepository.GetUserByEmailOrPhoneAndPassword(emailOrPhone, password);
            if (dbUser == null) return null;
            var mapper = _config.CreateMapper();
            return mapper.Map<User, AppUser>(dbUser);
        }

        public virtual bool IsPhoneNumberExist(string phoneNumber)
        {
            return _userRepository.IsPhoneNumberExist(phoneNumber);
        }

        public virtual bool IsEmailExist(string email)
        {
            return _userRepository.IsEmailExist(email);
        }
    }
}
