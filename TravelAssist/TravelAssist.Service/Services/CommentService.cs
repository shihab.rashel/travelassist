﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using TravelAssist.AppModel;
using TravelAssist.AppModel.AppModel;
using TravelAssist.DAL.Models;
using TravelAssist.Service.Interfaces;

namespace TravelAssist.Service.Services
{
    public partial class CommentService : GenericCrudService, ICommentService
    {
        private readonly MapperConfiguration _config;
        public CommentService(string connectionString) : base(connectionString)
        {
            _config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Comment, AppComment>();
                cfg.CreateMap<AppComment, Comment>();
            });
        }

        public virtual CrudResult Add(AppComment appComment)
        {
            var mapper = _config.CreateMapper();
            var comment = mapper.Map<AppComment, Comment>(appComment);
            return Add(comment);
        }
        
        public virtual List<AppComment> GetCommentByTouristPlaceId(int touristPlaceId)
        {
            var mapper = _config.CreateMapper();
            var dbCommentsOfThisPlace = GetByProperty<Comment>(c => c.TouristPlaceId == touristPlaceId);
            var appCommentsOfThisPlace = dbCommentsOfThisPlace.Select(c => mapper.Map<Comment, AppComment>(c)).ToList();
            return appCommentsOfThisPlace;
        }
    }
}
