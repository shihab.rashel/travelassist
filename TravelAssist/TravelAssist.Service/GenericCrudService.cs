﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using TravelAssist.AppModel;
using TravelAssist.DAL.Repository;

namespace TravelAssist.Service
{
    public partial class GenericCrudService:IGenericCrudService
    {
        private readonly IGenericCrudRepository _genericCrudRepository;
        public GenericCrudService(string connectionString)
        {
            _genericCrudRepository = new GenericCrudRepository(connectionString);
        }
        public virtual T Get<T>(int id) where T : class
        {
            return _genericCrudRepository.Get<T>(id);
        }

        public virtual List<T> GetAll<T>() where T : class
        {
            return _genericCrudRepository.GetAll<T>();
        }

        public virtual List<T> GetByProperty<T>(Expression<Func<T, bool>> predicate) where T : class
        {
            return _genericCrudRepository.GetByProperty(predicate);
        }

        public virtual CrudResult Add<T>(T entity) where T : class
        {
            try
            {
                var result = _genericCrudRepository.Add(entity);
                return new CrudResult
                {
                    Success = true,
                    Message = "Add Successful",
                    Entity = result
                };
            }
            catch (Exception exception)
            {
                return new CrudResult
                {
                    Success = false,
                    Message = exception.ToString(),
                    ErrorMessage = exception.ToString(),
                    Entity = null
                };
            }
        }

        public virtual CrudResult Edit<T>(T entity) where T : class
        {
            try
            {
                var result = _genericCrudRepository.Edit(entity);
                return new CrudResult
                {
                    Success = true,
                    Message = "Edit Successful",
                    Entity = result
                };
            }
            catch (Exception exception)
            {
                return new CrudResult
                {
                    Success = false,
                    Message = exception.ToString(),
                    ErrorMessage = exception.ToString(),
                    Entity = null
                };
            }
        }

        public virtual CrudResult Delete<T>(int id) where T : class
        {
            try
            {
                _genericCrudRepository.Delete<T>(id);
                return new CrudResult
                {
                    Success = true,
                    Message = "Delete Successful",
                    Entity = null
                };
            }
            catch (Exception exception)
            {
                return new CrudResult
                {
                    Success = false,
                    Message = exception.ToString(),
                    ErrorMessage = exception.ToString(),
                    Entity = null
                };
            }
        }
    }
}
